﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace H.W_Asher_03
{
    public partial class Form1 : Form
    {
        Panel P1, P2, P3;
        Graphics G1, G2, G3;
        Color[] ColorArr = { Color.Orange, Color.Brown, Color.Red, Color.Green, Color.Blue, Color.Pink, Color.Gray};
        Color change_color = Color.Black;
        bool rectangle = false;
        bool ellipse = false;
        int last_y = 0;
        int x1_Click = 0;
        int y1_Click = 0;
        int x2_Click = 0;
        int y2_Click = 0;
        bool full = false;
        bool line = false;
        public Form1()
        {
            InitializeComponent();

            P1 = new Panel();
            P2 = new Panel();
            P3 = new Panel();

            drawBord(P1, Color.White, this.Width, 0, 2000, 100);
            drawBord(P2, Color.Blue, this.Width, 100, 100, 1000);
            drawBord(P3, Color.Gray, this.Width, 100, 2000, 2000);

            this.Controls.Add(P1);
            this.Controls.Add(P2);
            this.Controls.Add(P3);

            G1 = P1.CreateGraphics();
            G2 = P2.CreateGraphics();
            G3 = P3.CreateGraphics();

            P2.MouseClick += P11_PanelClick;
            
            P1.MouseClick += P1_PanelClice;
            P3.MouseClick += P3_PanelClick;

            this.Paint += Panel2_Paint;
            this.Paint += Panel1_Paint;
        }

        private void P3_PanelClick(object sender, MouseEventArgs e)
        {
            if (x1_Click == 0 && y1_Click == 0)
            {
                x1_Click = e.X;
                y1_Click = e.Y;
            }
            else if ((x1_Click != 0 || y1_Click != 0) && (x2_Click == 0 && y2_Click == 0))
            {
                x2_Click = e.X;
                y2_Click = e.Y;
            }
            else
            {
                x2_Click = 0;
                y2_Click = 0;
                x1_Click = e.X;
                y1_Click = e.Y;
            }
            SolidBrush sb = new SolidBrush(change_color);
            Pen p = new Pen(change_color, 3);
            if (rectangle && (x1_Click != 0 || y1_Click != 0) && (x2_Click != 0 || y2_Click != 0))
            {
                if (full)
                {
                    G3.FillRectangle(sb, Math.Min(x1_Click, x2_Click), Math.Min(y1_Click, y2_Click), Math.Abs(x2_Click - x1_Click), Math.Abs(y2_Click - y1_Click));
                }
                else
                {
                    G3.DrawRectangle(p, Math.Min(x1_Click, x2_Click), Math.Min(y1_Click, y2_Click), Math.Abs(x2_Click - x1_Click), Math.Abs(y2_Click - y1_Click));
                }
            }
            if (ellipse && (x1_Click != 0 || y1_Click != 0) && (x2_Click != 0 || y2_Click != 0))
            {
                if (full)
                {
                    G3.FillEllipse(sb, Math.Min(x1_Click, x2_Click), Math.Min(y1_Click, y2_Click), Math.Abs(x2_Click - x1_Click), Math.Abs(y2_Click - y1_Click));
                }
                else
                {
                    G3.DrawEllipse(p, Math.Min(x1_Click, x2_Click), Math.Min(y1_Click, y2_Click), Math.Abs(x2_Click - x1_Click), Math.Abs(y2_Click - y1_Click));
                }
            }
            if (line && (x1_Click != 0 || y1_Click != 0) && (x2_Click != 0 || y2_Click != 0))
            {
                G3.DrawLine(p, x1_Click, y1_Click, x2_Click, y2_Click);
            }
        }

        private void P1_PanelClice(object sender, MouseEventArgs e)
        {
            int x_Click = e.X;
            SolidBrush sb;
            Pen p;
            sb = new SolidBrush(Color.White);
            G1.FillRectangle(sb, 0, 0, 100, 100);

            sb = new SolidBrush(change_color);
            p = new Pen(change_color, 3);
            if ((int)(x_Click / 100) == 1)
            {
                full = false;
                line = false;
                ellipse = false;
                rectangle = true;
                G1.DrawRectangle(p, 0, 0, 99, 99);
            }
            if ((int)(x_Click / 100) == 2)
            {
                full = false;
                line = false;
                ellipse = true;
                rectangle = false;
                G1.DrawEllipse(p, 0, 0, 99, 99);
            }
            if ((int)(x_Click / 100) == 3)
            {
                full = true;
                ellipse = false;
                rectangle = true;
                line = false;
                G1.FillRectangle(sb, 0, 0, 99, 99);
            }
            if ((int)(x_Click / 100) == 4)
            {
                full = true;
                line = false;
                rectangle = false;
                ellipse = true;
                G1.FillEllipse(sb, 0, 0, 99, 99);
            }
            if ((int)(x_Click / 100) == 5)
            {
                
                ellipse = false;
                rectangle = false;
                full = false;
                line = true;
                G1.DrawLine(p, 0, 50, 100, 50);
            }
            
        }

        private void P11_PanelClick(object sender, MouseEventArgs e)
        {
            int y_Click = e.Y;
            Pen p;
            //delite last mark.
            p = new Pen(ColorArr[(int)(last_y / 100)], 3);
            G2.DrawRectangle(p, 0, (int)(last_y / 100) * 100, 100, 100);

            //update new mark.
            change_color = ColorArr[(int)(y_Click / 100)];
            p = new Pen(Color.Black, 3);
            G2.DrawRectangle(p, 0, (int)(y_Click / 100) * 100, 100, 100);
            last_y = y_Click;
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush sb;
            sb = new SolidBrush(Color.White);

            for (int i = 0; i < ColorArr.Length; i++)
            {
              sb = new SolidBrush(ColorArr[i]);
              G2.FillRectangle(sb, 0, i * 100, 100, 100);
            }
        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.Black, 10);
            SolidBrush sb = new SolidBrush(Color.Black);
            //
            G1.DrawRectangle(p, 100, 0, 100, 100);
            G1.DrawEllipse(p, 200, 0, 100, 100);
            //
            G1.FillRectangle(sb, 300, 0, 100, 100);
            G1.FillEllipse(sb, 400, 0, 100, 100);
            //
            G1.DrawLine(p, 520, 50, 600, 50);
        }


        private void drawBord(Panel p,Color cl ,int x, int y, int width, int height)
        {
            p.Location = new Point(this.Width - x, y);
            p.Size = new Size(this.Width - 40, this.Height - 40);
            p.BackColor = cl;
            p.Width = width;
            p.Height = height;
        }
    }
}
